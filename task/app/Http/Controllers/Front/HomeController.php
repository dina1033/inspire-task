<?php

namespace App\Http\Controllers\Front;
use App\Post;
use App\User;
use App\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showposts(){
        return view('front.home',[
            'posts' => Post::all(),
            'pages'=>Page::all()
        ]);

    }

    public function showpages(Post $post , Page $page){
        return view ('front.detailsposts',[
            'post' =>$post,
            'page'=>$page
        ]);
    }
}
