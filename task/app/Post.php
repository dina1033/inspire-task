<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'img', 'description','user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class ,'user_id');
    }
    public function page(){
        return $this->belongsTo(Page::class,'post_id');
    }
    public function latestPost()
{
    return $this->hasOne(Post::class)->latest();
}
}
