<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'img', 'description','post_id'
    ];
    
    public function post(){
        return $this->hasOne(Post::class,'post_id');
    }
}
