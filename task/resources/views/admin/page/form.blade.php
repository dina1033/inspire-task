@csrf

<div class="form-group">
    <label for="post_id"> post title</label>
    <select name="post_id" id="post_id" class="form-control">
        <option value>Not set</option>
        @foreach($posts as $post) 
            @if(isset($page))
                <option value="{{ $post->id }}" selected>{{ $post->title }}</option>
            @else
                <option value="{{ $post->id }}" >{{ $post->title }}</option>  
            @endif
        @endforeach
    </select>
    
</div>

<div class="form-group">
    <label for="titile">Title</label>
    <input type="text" name="title" id="name" class="form-control" value="{{ isset($page)?$page->title : '' }}">
    

</div>

<div class="form-group">
    <label for="img">img</label>
    <input type="file" name="img" id="img" class="form-control" 
    value="{{ isset($page)? $page->post->img : '' }}">
   

</div>


<div class="form-group">
<textarea class="ckeditor form-control" name="wysiwyg-editor">{!! isset($page)?$page->description : '' !!}</textarea>
   
</div>


<div class="text-center">
    <button type="submit" class="btn btn-success">Save</button>
</div>
