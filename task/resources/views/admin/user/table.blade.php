
@foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td><img  style="width: 90px; height: 90px;" src="{{asset('images/users/'.$user->img)}}"></td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                       
                                        <a href="{{route('admin.users.edit', $user)}}" class="btn btn-primary">Edit</a>
                                        
                                        <button type="button" class="btn btn-danger delete" data-url="{{ route('admin.users.destroy', $user) }}">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                          