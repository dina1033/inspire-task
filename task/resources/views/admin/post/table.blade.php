
@foreach($posts as $post)
    <tr>
        <td>{{ $post->id }}</td>
        <td>
            @if ($post->user)
                {{ $post->user->name }}
            @endif
        </td>
        <td>{{ $post->title }}</td>
        <td><img  style="width: 90px; height: 90px;" src="{{asset('images/posts/'.$post->img)}}"></td>
        <td>{!! $post->description !!}</td>
        <td>
            
            <a href="{{route('admin.posts.edit', $post)}}" class="btn btn-primary">Edit</a>
                                        
            <button type="button" class="btn btn-danger delete" data-url="{{ route('admin.posts.destroy', $post) }}">Delete</button>
        </td>
    </tr>
@endforeach
                          