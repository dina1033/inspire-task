@csrf
<div class="form-group">
    <label for="user_id"> User name</label>
    <select name="user_id" id="user_id" class="form-control">
        <option value>Not set</option>
        @foreach($users as $user) 
            @if(isset($post))
                <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
            @else
                <option value="{{ $user->id }}" >{{ $user->name }}</option>  
            @endif
        @endforeach
    </select>
    
</div>

<div class="form-group">
    <label for="titile">Title</label>
    <input type="text" name="title" id="name" class="form-control" value="{{ isset($post)?$post->title : '' }}">
    

</div>

<div class="form-group">
    <label for="img">img</label>
    <input type="file" name="img" id="quntity" class="form-control" 
    value="{{ isset($post)? $post->img : '' }}">
   

</div>


<div class="form-group">
<textarea class="ckeditor form-control" name="wysiwyg-editor">{!! isset($post)?$post->description : '' !!}</textarea>
   
</div>



<div class="text-center">
    <button type="submit" class="btn btn-success">Save</button>
</div>
