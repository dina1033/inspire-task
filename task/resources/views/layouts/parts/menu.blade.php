<li class="nav-item">
    <a href="{{ route('admin.dashboard') }}"
       class="nav-link {{ Route::currentRouteName() == 'admin.dashboard'? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer"></i>
        <p>
            Dashboard
        </p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.users.index') }}"
       class="nav-link {{ Route::currentRouteName() == 'admin.users.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-user"></i>
        <p>
            Users
        </p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.posts.index') }}"
       class="nav-link {{ Route::currentRouteName() == 'admin.posts.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-comments-o"></i>
        <p>
            Posts
        </p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.pages.index') }}"
       class="nav-link {{ Route::currentRouteName() == 'admin.pages.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-file-text"></i>
        <p>
            Pages
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="#"
       class="nav-link setting {{ Route::currentRouteName() == '#'? 'active' : '' }}">
        <i class="nav-icon fa fa-cog"></i>
        <p>
            Setting
        </p>
    </a>
    <div class="setpage">
    </div>
</li>

