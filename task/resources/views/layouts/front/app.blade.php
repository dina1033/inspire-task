<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ config('app.name') }} | @yield('title')</title>
    @include('layouts.parts.css')
<body class="w3-theme-l5">

@include('layouts.front.parts.hearder')


@yield('content')


@include('layouts.front.parts.footer')



@include('layouts.front.parts.js')
@yield('js')
</body>
</html> 