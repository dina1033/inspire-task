<!DOCTYPE html>
<html>
<title>posts</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {font-family: "Open Sans", sans-serif}
</style>
<body class="w3-theme-l5">

<!-- Navbar -->
<div class="w3-top">
 <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
  <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large w3-theme-d4"><i class="fa fa-home w3-margin-right"></i>Logo</a>
  <a href="#" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" onclick="openNav()">
  <i class="fa fa-bars"></i>
 
  </a>

 </div>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-large  w3-hide" >
@foreach($pages as $page)
  <a href="#" class="w3-bar-item w3-button w3-padding-large">{{$page->title}}</a>
 @endforeach
</div>

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top:80px">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m2">
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m8 " style="margin-left: 207px;"> 
    @foreach($posts as $post)    
      <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
      <img  style="width: 60px;"class="w3-left w3-circle w3-margin-right"alt="Avatar" src="{{asset('images/users/'.$post->user->img)}}">       
        <h4>{{$post->user->name}}</h4><br>
        <hr class="w3-clear">
        <h5>{{$post->title}}</h5>
        <p style="margin-top: 47px">{!! \Illuminate\Support\Str::limit($post->description ,50,'...')!!}<a href="{{route('pagesdetails', $post)}}" class='btn btn-primary'>Read More</a></p>
          <div class="w3-row-padding" style="margin:0 -16px">
            <div class="w3-half "style="margin-top: 41px;">
            <img  style="width:100%"class="w3-margin-bottom"alt="Northern Lights" src="{{asset('images/posts/'.$post->img)}}">
              
            </div>           
          </div>
 
      </div>
    
      @endforeach
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      
      
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
<br>

<!-- Footer -->
<footer class="w3-container w3-theme-d3 w3-padding-16 w3-center ">
<strong>Copyright &copy; 2014-2019 </strong> All rights reserved.
</footer>


 
<script>
// Accordion
function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    x.previousElementSibling.className += " w3-theme-d1";
  } else { 
    x.className = x.className.replace("w3-show", "");
    x.previousElementSibling.className = 
    x.previousElementSibling.className.replace(" w3-theme-d1", "");
  }
}

// Used to toggle the menu on smaller screens when clicking on the menu button
function openNav() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html> 
