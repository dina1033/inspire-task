<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/', 'Front\HomeController@showposts');
    Route::get('/pages', 'Front\HomeController@showpages')->name('pages');
    Route::get('/pagesdetails/{post}', 'Front\HomeController@showpages')->name('pagesdetails');

Route::group([
    'prefix' => '/admin',
    'as' => 'admin.',
    'middleware' => ['auth' ]
], function () {
Route::get('/', 'DashboardController')->name('dashboard');
     

//route users
Route::group([
    'prefix' => 'users',
    'as' => 'users.'
], function () {
    Route::get('/get-users', 'UserController@getUsers')->name('get-users');
});
Route::resource('/users', 'UserController');

//route posts

Route::group([
    'prefix' => 'posts',
    'as' => 'posts.'
], function () {
    Route::get('/get-posts', 'PostController@getPosts')->name('get-posts');
});
Route::resource('/posts', 'PostController');

//route pages

Route::group([
    'prefix' => 'pages',
    'as' => 'pages.'
], function () {
    Route::get('/get-pages', 'PageController@getPages')->name('get-pages');
});
Route::resource('/pages', 'PageController');

});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
